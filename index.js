import 'dotenv/config';
import Fastify from 'fastify';
import dialogflow from '@google-cloud/dialogflow';
import got  from 'got';

const fastify = Fastify({
  logger: {
    prettyPrint:
        process.env.NODE_ENV === 'development'
        ? {
            translateTime: 'HH:MM:ss Z',
            ignore: 'pid,hostname'
          }
        : false
  }
});

const projectId = process.env.GOOGLE_PROJECT_ID;
const languageCode = process.env.GOOGLE_PROJECT_LANGUAGE;

const WAFVEL_PHONE_NUMBER = process.env.WAFVEL_PHONE_NUMBER;
const WAFVEL_BOT_TOKEN = process.env.WAFVEL_BOT_TOKEN;
const WAFVEL_USER_TOKEN = process.env.WAFVEL_USER_TOKEN;
const WAFVEL_ENDPOINT = `${process.env.WAFVEL_ENDPOINT}/${WAFVEL_PHONE_NUMBER}`;

async function detectIntent(
  projectId,
  sessionId,
  query,
  contexts,
  languageCode,
  sessionClient
) {
  // The path to identify the agent that owns the created intent.
  const sessionPath = sessionClient.projectAgentSessionPath(
    projectId,
    sessionId
  );

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: query,
        languageCode: languageCode,
      },
    },
  };

  if (contexts && contexts.length > 0) {
    request.queryParams = {
      contexts: contexts,
    };
  }

  const responses = await sessionClient.detectIntent(request);
  return responses[0];
}

async function executeQueries(projectId, sessionId, queries, languageCode, sessionClient) {
  // Keeping the context across queries let's us simulate an ongoing conversation with the bot
  let context;
  let intentResponse;
  for (const query of queries) {
    try {
      fastify.log.info(`Sending Query: ${query}`);
      intentResponse = await detectIntent(
        projectId,
        sessionId,
        query,
        context,
        languageCode,
        sessionClient
      );
      fastify.log.info('Detected intent');
      fastify.log.info(
        `Fulfillment Text: ${intentResponse.queryResult.fulfillmentText}`
      );
      // Use the context from this response for next queries
      context = intentResponse.queryResult.outputContexts;

      return intentResponse.queryResult.fulfillmentText
    } catch (error) {
      fastify.log.info(error);
    }
  }
}

// Declare a route
fastify.post('/', async function (request, reply) {
    const sessionClient = new dialogflow.SessionsClient();
    const sessionId = request.body.participants.sender.replace('@c.us','');
    const queries = [request.body.message];

    if (!request.body.fromMe && request.body.participants.sender !== 'status@broadcast') {
        const fulfillment = await executeQueries(projectId, sessionId, queries, languageCode, sessionClient);

        const { message } = await got.post(`${WAFVEL_ENDPOINT}/sendMessage`, {
            json: {
                token: WAFVEL_BOT_TOKEN,
                phone: request.body.participants.sender.replace('@c.us',''),
                message: fulfillment
            },
            headers: { 
                'Authorization': `Bearer ${WAFVEL_USER_TOKEN}`,
              },
        }).json();
        fastify.log.info(message);
        reply.send({ fulfillment })
    } else {
        fastify.log.info('message is from me, skip this')
        reply.send({ message: 'message is from me, skip this.' })
    }
})

// Run the server!
fastify.listen(3000, async function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
})
