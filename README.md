This is initial demo of wafvel whatsapp API + Dialogflow ES Integration using Fastify as webhook server.

based on: https://cloud.google.com/dialogflow/es/docs/quick/api

![](2022-05-27-08-45-40.png)

When chat received on whatsapp bot, wafvel will send the message using wafvel webhook service to this service, this service processing the message with Dialogflow ES, then reply back using wafvel API.


Webhook on Wafvel
![](2022-05-27-08-35-15.png)

How to run:
1. Clone this repo and run `yarn`
2. Create `.env` file or copy from `.env.example` and fill with your credentials from wafvel
3. Get json credential from GCP and add in the root folder. See `credentials.example.json` or more details [here](https://cloud.google.com/docs/authentication/getting-started)
4. Rename as credentials.json on the file from the step 3 and run
```bash
export GOOGLE_APPLICATION_CREDENTIALS=credentials.json
```
5. You can use ngrok to enable public access to this dev
```bash
ngrok http 3000
```
example:
```bash
Forwarding: https://720c-125-167-83-218.ap.ngrok.io
```
6. Use the forwarding url at the wafvel webhook on the bot setting.
7. run this service with
```bash
yarn start
```
8. Chat to your bot, it should be running and reply to your chat using fulfillment from Dialogflow ES.
